// This is a manifest file that'll be compiled into application.js, which will include all the files
// listed below.
//
// Any JavaScript/Coffee file within this directory, lib/assets/javascripts, vendor/assets/javascripts,
// or any plugin's vendor/assets/javascripts directory can be referenced here using a relative path.
//
// It's not advisable to add code directly here, but if you do, it'll appear at the bottom of the
// compiled file.
//
// Read Sprockets README (https://github.com/sstephenson/sprockets#sprockets-directives) for details
// about supported directives.
//
//= require jquery
//= require jquery_ujs
//= require turbolinks
//= require_tree .

$(document).ready(function(){
  ROCK = 0;
  PAPER = 1;
  SCISSORS = 2;
  TIE = 3;

  var pastGame = []//list of lists, the list [player, computer, winner]

  var computerWins = 0;
  var playerWins = 0;
  var ties = 0;

  PLAYER = 0;
  COMPUTER = 1;
  WINNER = 2;

  winnerOutput = $('#winner')

  computer_choose = $('#computer_choose')
  player_choose = $('#player_choose')

  $('#rock').click(function() {
    runGame(ROCK);
  })

  $('#paper').click(function() {
    runGame(PAPER);
  })

  $('#scissors').click(function() {
    runGame(SCISSORS);
  })

  function runGame(playerHand) {
    computerHand = pickHand();
    winner = findWinner(playerHand, computerHand);
    updateScoreboard(playerHand, computerHand, winner)
    pastGame = [playerHand, computerHand, winner];
  }


  function pickHand() {
    if(pastGame.length == 0) {
      //return Math.floor(Math.random() * 3)
      return ROCK;
    } else {
      if(pastGame[WINNER] == COMPUTER) { // computer wins
        return pastGame[PLAYER]
      } else if(pastGame[WINNER] == PLAYER) { // computer doesn't win
        switch(pastGame[PLAYER]) {
          case ROCK: return PAPER; break;
          case PAPER: return SCISSORS; break;
          case SCISSORS: return ROCK; break;
        }
      }
      else {
        return Math.floor(Math.random() * 3)
      }
    }
    
  }

  function updateScoreboard(playerHand, computerHand, winner) {
    switch(winner) {
      case PLAYER: playerWins += 1; break;
      case COMPUTER: computerWins += 1; break;
      case TIE: ties += 1; break;
    }
    $('#player_wins').text(playerWins);
    $('#computer_wins').text(computerWins);
    $('#ties').text(ties);
    $('#ratio').text(playerWins/computerWins)
    if(winner == COMPUTER) {
      winnerOutput.text("I won");
      winnerOutput.css('color', 'red')
    } else if (winner == PLAYER) {
      winnerOutput.text("you won...");
      winnerOutput.css('color', 'blue')
    }
    else {
      winnerOutput.text("it's a tie");
      winnerOutput.css('color', 'black')
    }
    
    player_choose.text(handToString(playerHand))
    computer_choose.text(handToString(computerHand)) 

  }

  function findWinner(playerHand, computerHand) {
    if(playerHand == ROCK) {
      switch(computerHand) {
        case ROCK: return TIE;
        case PAPER: return COMPUTER;
        case SCISSORS: return PLAYER;
      }
    } else if(playerHand == PAPER) {
      switch(computerHand) {
        case ROCK: return PLAYER;
        case PAPER: return TIE;
        case SCISSORS: return COMPUTER;
      }
    } else {
      switch(computerHand) {
        case ROCK: return COMPUTER;
        case PAPER: return PLAYER;
        case SCISSORS: return TIE;
      }
    }
  }

  function handToString(hand) {
    switch(hand) {
      case ROCK: return 'rock';
      case PAPER: return 'paper';
      case SCISSORS: return 'scissors';
    }
  }




  
});